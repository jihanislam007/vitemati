package md.mazharul.islam.jihan.vitemati.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import md.mazharul.islam.jihan.vitemati.Activity.MainActivity;
import md.mazharul.islam.jihan.vitemati.DB.OfflineInfo;
import md.mazharul.islam.jihan.vitemati.Fragment.LogInFragment;
import md.mazharul.islam.jihan.vitemati.ModelClass.DashBoardListDM;
import md.mazharul.islam.jihan.vitemati.Fragment.DetailsViewFragment;
import md.mazharul.islam.jihan.vitemati.R;

/*////////////////ListView Adapter//////////////*/
public class DashBoardAdapter extends ArrayAdapter {

        Context mcontext;
        ArrayList<DashBoardListDM> dashBoardListDMArrayList;

        public DashBoardAdapter(Context mcontext, ArrayList<DashBoardListDM> dashBoardListDMArrayList) {
            super(mcontext, R.layout.raw_dashboard_listview);
            this.mcontext = mcontext;
            this.dashBoardListDMArrayList = dashBoardListDMArrayList;

        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {

            //android.view.LayoutInflater inflater = context.getLayoutInflater();
            LayoutInflater inflater = LayoutInflater.from(mcontext);

            View rowView= inflater.inflate(R.layout.raw_dashboard_listview, null, true);


            TextView land_owner_name = (TextView) rowView.findViewById(R.id.land_owner_name);
            TextView land_owner_address = (TextView) rowView.findViewById(R.id.land_owner_address);
            TextView land_type = (TextView) rowView.findViewById(R.id.land_type);

            LinearLayout linearLayout = rowView.findViewById(R.id.detailsView);
            /*ImageView backgroundImageView = (ImageView) rowView.findViewById(R.id.backgroundImageView);

            Glide.with(mcontext)
                    .load(categoryItemList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(backgroundImageView);*/

            land_owner_name.setText(dashBoardListDMArrayList.get(position).getOwner_name());
            land_owner_address.setText(dashBoardListDMArrayList.get(position).getDistrict());
            land_type.setText(dashBoardListDMArrayList.get(position).getLandtype());

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    android.os.Bundle bundle=new android.os.Bundle();

                    bundle.putString("owner_name", dashBoardListDMArrayList.get(position).getOwner_name());
                    bundle.putString("owner_phone", dashBoardListDMArrayList.get(position).getOwner_phone());
                    bundle.putString("owner_nid",dashBoardListDMArrayList.get(position).getOwner_nid());
                    bundle.putString("owner_email",dashBoardListDMArrayList.get(position).getOwner_email());

                    bundle.putString("house_no",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("road_no",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("village",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("post_office",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("union_ward_no",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("property_size",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("property_price",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("mouza_name",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("jl_no",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("khatian",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("dag_no",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("natureofland",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("surroundingofland",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("longitude",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("latitude",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("googlemap_address",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("district",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("division",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("policestation",dashBoardListDMArrayList.get(position).getDivision());
                    bundle.putString("landtype",dashBoardListDMArrayList.get(position).getDivision());


                    OfflineInfo offlineInfo=new OfflineInfo(mcontext);

                    if(offlineInfo.getUserInfo()==null){

                        Bundle pageInformationBundle=new Bundle();
                        pageInformationBundle.putString("page_name","DetailsFragment");
                        Gson gson=new Gson();
                        String data=gson.toJson(dashBoardListDMArrayList.get(position));
                        pageInformationBundle.putString("page_data",data);

                        LogInFragment logInFragment=new LogInFragment();
                        logInFragment.setArguments(pageInformationBundle);

                        FragmentTransaction fragTransaction = ((MainActivity)mcontext).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,logInFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }else{
                        DetailsViewFragment detailsViewFragment = new DetailsViewFragment();
                        detailsViewFragment.setArguments(bundle);

                        FragmentTransaction fragTransaction = ((MainActivity)mcontext).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,detailsViewFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }



                }
            });

            /*/////////// Font path ////////////////////

            // Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nikosh.ttf");
            Typeface font1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/SolaimanLipi.ttf");
            categoryItemNameTV.setTypeface(font1);

            ///////////////////// xx //////////////////////*/

            return rowView;
        }

        @Override
        public int getCount() {
            return dashBoardListDMArrayList.size();
        }
    }