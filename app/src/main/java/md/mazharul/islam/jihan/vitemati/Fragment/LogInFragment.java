package md.mazharul.islam.jihan.vitemati.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import md.mazharul.islam.jihan.vitemati.Activity.MainActivity;
import md.mazharul.islam.jihan.vitemati.DB.OfflineInfo;
import md.mazharul.islam.jihan.vitemati.ModelClass.DashBoardListDM;
import md.mazharul.islam.jihan.vitemati.R;
import md.mazharul.islam.jihan.vitemati.ServerInfo.ServerInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class LogInFragment extends Fragment {


    public LogInFragment() {
        // Required empty public constructor
    }

    EditText ETMobileNoLogin,
            ETPasswordLogin;
    Button btnSellerLogin;
    TextView seller_loginTextView;

    OfflineInfo offlineInfo;

    String currentPage="";
    String prevpageData="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_log_in, container, false);
        try {
            currentPage=getArguments().getString("page_name");
            prevpageData=getArguments().getString("page_data");
        } catch (Exception e) {

        }
        ETMobileNoLogin = rootView.findViewById(R.id.ETMobileNoLogin);
        ETPasswordLogin = rootView.findViewById(R.id.ETPasswordLogin);

        btnSellerLogin = rootView.findViewById(R.id.btnSellerLogin);

        offlineInfo = new OfflineInfo(getActivity());

       /* //Auto login if previously login
        if(offlineInfo.getUserInfo()!=null && offlineInfo.getUserInfo().token!=null && offlineInfo.getUserInfo().token.length()>0){
            *//*Intent intent=new Intent(LogInActivity.this,UploadSmsCatagorySelectorActivity.class);
            startActivity(intent);
            finish();*//*
        }*/

        btnSellerLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Login(ETMobileNoLogin.getText().toString(), ETPasswordLogin.getText().toString());
            }
        });

        /*///////////////////////////////////////////////////////////*/
        seller_loginTextView = rootView.findViewById(R.id.seller_loginTextView);

        seller_loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegistrationFragment registrationFragment = new RegistrationFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_container, registrationFragment);
                fragmentTransaction.commit();
            }
        });

        return rootView;
    }

    private void Login(final String email, final String password) {
        String tag_string_req = "req_login";
        ProgressDialog progressDialog = null;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        final ProgressDialog finalProgressDialog = progressDialog;

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.add("Email", email);
        params.add("Password", password);

        final ProgressDialog finalProgressDialog1 = progressDialog;
        try {
            String url=ServerInfo.BASE_ADDRESS+"login?username=" + URLEncoder.encode(email, "UTF-8")+"&password="+URLEncoder.encode(password,"UTF-16");
            //String url="http://vitematy.com/api/login";
            Log.e("Url",url);
            client.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                    System.out.println(response);
                    Log.e("Response ",response.toString());
                    offlineInfo.setUserInfo(response.toString());
                    if(currentPage.equals("DetailsFragment")){


                        Gson gson =new Gson();
                        DashBoardListDM dashboardData=gson.fromJson(prevpageData,DashBoardListDM.class);


                        Bundle bundle=new Bundle();

                        bundle.putString("owner_name", dashboardData.getOwner_name());
                        bundle.putString("owner_phone", dashboardData.getOwner_phone());
                        bundle.putString("owner_nid",dashboardData.getOwner_nid());
                        bundle.putString("owner_email",dashboardData.getOwner_email());

                        bundle.putString("house_no",dashboardData.getDivision());
                        bundle.putString("road_no",dashboardData.getDivision());
                        bundle.putString("village",dashboardData.getDivision());
                        bundle.putString("post_office",dashboardData.getDivision());
                        bundle.putString("union_ward_no",dashboardData.getDivision());
                        bundle.putString("property_size",dashboardData.getDivision());
                        bundle.putString("property_price",dashboardData.getDivision());
                        bundle.putString("mouza_name",dashboardData.getDivision());
                        bundle.putString("jl_no",dashboardData.getDivision());
                        bundle.putString("khatian",dashboardData.getDivision());
                        bundle.putString("dag_no",dashboardData.getDivision());
                        bundle.putString("natureofland",dashboardData.getDivision());
                        bundle.putString("surroundingofland",dashboardData.getDivision());
                        bundle.putString("longitude",dashboardData.getDivision());
                        bundle.putString("latitude",dashboardData.getDivision());
                        bundle.putString("googlemap_address",dashboardData.getDivision());
                        bundle.putString("district",dashboardData.getDivision());
                        bundle.putString("division",dashboardData.getDivision());
                        bundle.putString("policestation",dashboardData.getDivision());
                        bundle.putString("landtype",dashboardData.getDivision());
                        


                        DetailsViewFragment detailsViewFragment = new DetailsViewFragment();
                        detailsViewFragment.setArguments(bundle);

                        FragmentTransaction fragTransaction = ((MainActivity)getContext()).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,detailsViewFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }else  if(currentPage.equals("seller_land")){
                        LandSaleFragment landSaleFragment = new LandSaleFragment();
                        FragmentTransaction fragTransaction = ((MainActivity)getContext()).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,landSaleFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }
                    else  if(currentPage.equals("buyer")){
                        LandBuyerFragment landBuyerFragment = new LandBuyerFragment();

                        FragmentTransaction fragTransaction = ((MainActivity)getContext()).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,landBuyerFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }else  if(currentPage.equals("add_land")){
                        NewLandAddFragment newLandAddFragment = new NewLandAddFragment();

                        android.support.v4.app.FragmentTransaction fragTransaction = ((MainActivity)getContext()).getSupportFragmentManager().beginTransaction();
                        fragTransaction.replace(R.id.frag_container,newLandAddFragment );
                        fragTransaction.addToBackStack(null);
                        fragTransaction.commit();
                    }


                }

                @Override
                public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    finalProgressDialog1.dismiss();
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                    Log.e("Response Debug","go to on failure");
                    System.out.println(statusCode);
                    System.out.println(responseString);
                    Log.e("Response Debug",responseString);
                    Toast.makeText(getActivity(), "User name or password invalid....", Toast.LENGTH_SHORT).show();

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        //client.post(ServerInfo.BASE_ADDRESS + "login", params, new JsonHttpResponseHandler() {


    }
}