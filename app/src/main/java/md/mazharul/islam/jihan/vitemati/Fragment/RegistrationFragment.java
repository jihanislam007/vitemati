package md.mazharul.islam.jihan.vitemati.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.HttpResponse;
import md.mazharul.islam.jihan.vitemati.DB.OfflineInfo;
import md.mazharul.islam.jihan.vitemati.R;
import md.mazharul.islam.jihan.vitemati.ServerInfo.ServerInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {


    public RegistrationFragment() {
        // Required empty public constructor
    }

    EditText ETMobileNoRegistration,
            ETPasswordRegistration;
    Button btnRegistration;
    TextView seller_loginTextView;
    OfflineInfo offlineInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);

        offlineInfo = new OfflineInfo(getActivity());

        ETMobileNoRegistration = rootView.findViewById(R.id.ETMobileNoRegistration);
        ETPasswordRegistration = rootView.findViewById(R.id.ETPasswordRegistration);
        btnRegistration = rootView.findViewById(R.id.btnRegistration);

        seller_loginTextView = rootView.findViewById(R.id.seller_loginTextView);

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SignUp_server(ETMobileNoRegistration.getText().toString(), ETPasswordRegistration.getText().toString());

            }
        });

        /*/////////////////////////////////////////////////////////////////*/

        seller_loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogInFragment logInFragment = new LogInFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_container, logInFragment);
                fragmentTransaction.commit();
            }
        });

        return rootView;
    }

    private void SignUp_server(String fullName, final String password) {
        String tag_string_req = "req_login";
        ProgressDialog progressDialog = null;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        final ProgressDialog finalProgressDialog = progressDialog;

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        //  params.add("Email",email);
        params.add("FullName", fullName);
        params.add("Password", password);

        final ProgressDialog finalProgressDialog1 = progressDialog;

        client.post(ServerInfo.BASE_ADDRESS + "CreateUser", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {

                /*offlineInfo.setUserInfo(response.toString());

                try {
                    if (response.getBoolean("result")) {
                        Toast.makeText(getActivity(), "Sign up successful", Toast.LENGTH_LONG).show();
                        Login(fullName, password);

                    } else {

                        Toast.makeText(getActivity(), response.getString("errorMsg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                finalProgressDialog1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), "User name or password invalid....", Toast.LENGTH_SHORT).show();

            }

        });

    }
}