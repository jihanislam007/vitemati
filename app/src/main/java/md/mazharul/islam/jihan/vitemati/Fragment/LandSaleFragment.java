package md.mazharul.islam.jihan.vitemati.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpResponse;
import md.mazharul.islam.jihan.vitemati.Adapter.DashBoardAdapter;
import md.mazharul.islam.jihan.vitemati.ModelClass.DashBoardListDM;
import md.mazharul.islam.jihan.vitemati.R;
import md.mazharul.islam.jihan.vitemati.ServerInfo.ServerInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class LandSaleFragment extends Fragment {


    public LandSaleFragment() {
        // Required empty public constructor
    }

    ListView LandSalerListview;
    ArrayList<DashBoardListDM> dashBoardListDMArrayList = new ArrayList<>();
    DashBoardAdapter dashBoardAdapter;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_land_sale, container, false);

        LandSalerListview = rootView.findViewById(R.id.LandSalerListview);

        dashBoardAdapter =new DashBoardAdapter(getContext(),dashBoardListDMArrayList);
        LandSalerListview.setAdapter(dashBoardAdapter);

        DashBoardDataServer();
       // testdata();

        return rootView;
    }

    private void DashBoardDataServer() {

        String tag_string_req = "req_login";
        ProgressDialog progressDialog = null;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        final ProgressDialog finalProgressDialog = progressDialog;

        AsyncHttpClient client = new AsyncHttpClient();
        //client.addHeader("Authorization","Bearer "+offlineInfo.getUserInfo().token);

        //   RequestParams params = new RequestParams();
        final ProgressDialog finalProgressDialog1 = progressDialog;
        client.get(ServerInfo.BASE_ADDRESS + "land_list_seller?seller_id=20" , new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {

                dashBoardListDMArrayList.clear();

                //    SubCatSMS_viewAdapter.notifyDataSetChanged();
                Log.e("response",response.toString());
                System.out.print(response);

                /*...............////////////.....................*/

                //jsonObject = response.getJSONObject("list");
                try {
                    JSONObject jsonObject = response.getJSONObject("list");

                    String owner_name =jsonObject.getString("owner_name");
                    String owner_phone =jsonObject.getString("owner_phone");
                    String owner_nid =jsonObject.getString("owner_nid");

                    String owner_email =jsonObject.getString("owner_email");
                    String house_no =jsonObject.getString("house_no");
                    String road_no =jsonObject.getString("road_no");
                    String village =jsonObject.getString("village");
                    String post_office =jsonObject.getString("post_office");
                    String union_ward_no =jsonObject.getString("union_ward_no");
                    String property_size =jsonObject.getString("property_size");
                    String property_price =jsonObject.getString("property_price");
                    String mouza_name =jsonObject.getString("mouza_name");
                    String jl_no =jsonObject.getString("jl_no");
                    String khatian =jsonObject.getString("khatian");
                    String dag_no =jsonObject.getString("dag_no");
                    String natureofland =jsonObject.getString("natureofland");
                    String surroundingofland =jsonObject.getString("surroundingofland");
                    String longitude =jsonObject.getString("longitude");
                    String latitude =jsonObject.getString("latitude");
                    String googlemap_address =jsonObject.getString("googlemap_address");
                    String division =jsonObject.getString("division");
                    String district =jsonObject.getString("district");
                    String policestation =jsonObject.getString("policestation");
                    String landtype =jsonObject.getString("landtype");

                    DashBoardListDM dashBoardListDM = new DashBoardListDM(owner_name,owner_phone,owner_nid,
                            owner_email,house_no,road_no,village,post_office,union_ward_no,property_size,
                            property_price,mouza_name,jl_no,khatian,dag_no,natureofland,surroundingofland,
                            longitude,latitude,googlemap_address,division,district,policestation,landtype);

                    dashBoardListDMArrayList.add(dashBoardListDM);
                    // Log.e("getData",categoryList.size()+"--"+category_data.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }



                /*...............////////////.....................*/
                /*for (int i = 0; i < response.length(); i++) {
                    try {

                        JSONObject jsonObject = response.getJSONObject(i);
                        String catagoryname = jsonObject.getString("category_name");

                        int category_total_item = jsonObject.getInt("category_total_item");
                        //   int category_total_item = jsonObject.getInt("total");
                        String category_image = jsonObject.getString("category_image");
                        int category_id = jsonObject.getInt("category_id");

                        String owner_name = jsonObject.getString("owner_name");


                        *//*DashBoardListDM dashBoardListDM = new DashBoardListDM(owner_name,owner_phone,owner_nid,
                                owner_email,house_no,road_no,village,post_office,union_ward_no,property_size,
                                property_price,mouza_name,jl_no,khatian,dag_no,natureofland,surroundingofland,
                                longitude,latitude,googlemap_address,division,district,policestation,landtype;);

                        dashBoardListDMArrayList.add(dashBoardListDM);*//*
                        // Log.e("getData",categoryList.size()+"--"+category_data.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/
                dashBoardAdapter.notifyDataSetChanged();


            }

            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                finalProgressDialog1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), "There is no data", Toast.LENGTH_SHORT).show();

            }

        });
    }

    /*private void testdata() {

        DashBoardListDM a= new DashBoardListDM("owner_name","owner_phone",
                "owner_nid","owner_email","house_no","road_no",
                "village","post_office","union_ward_no", "property_size",                "property_price",                "mouza_name",                "jl_no",                "khatian",                "dag_no",                "natureofland",                "surroundingofland",                "longitude",                "latitude",                "googlemap_address",                "division",                "district",                "policestation",                "landtype");
        dashBoardListDMArrayList.add(a);

        DashBoardListDM c = new DashBoardListDM("ABC","owner_phone",
                "owner_nid","owner_email","house_no","road_no",
                "village","post_office","union_ward_no", "property_size",                "property_price",                "mouza_name",                "jl_no",                "khatian",                "dag_no",                "natureofland",                "surroundingofland",                "longitude",                "latitude",                "googlemap_address",                "division",                "district",                "policestation",                "landtype");
        dashBoardListDMArrayList.add(c);

        DashBoardListDM b = new DashBoardListDM("Gurar Dim","owner_phone",
                "owner_nid","owner_email","house_no","road_no",
                "village","post_office","union_ward_no", "property_size",                "property_price",                "mouza_name",                "jl_no",                "khatian",                "dag_no",                "natureofland",                "surroundingofland",                "longitude",                "latitude",                "googlemap_address",                "division",                "district",                "policestation",                "landtype");
        dashBoardListDMArrayList.add(b);
    }*/
}
