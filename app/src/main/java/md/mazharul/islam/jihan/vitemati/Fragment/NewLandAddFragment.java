package md.mazharul.islam.jihan.vitemati.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONObject;

import cz.msebera.android.httpclient.HttpResponse;
import md.mazharul.islam.jihan.vitemati.DB.OfflineInfo;
import md.mazharul.islam.jihan.vitemati.R;
import md.mazharul.islam.jihan.vitemati.ServerInfo.ServerInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewLandAddFragment extends Fragment {


    public NewLandAddFragment() {
        // Required empty public constructor
    }

    EditText editTextSellerName,
            editTextSellerPhone,
            editTextSellerNID,
            editTextSellerEmail,

            editTextHouseNo,
            editTextRoadNo,
            editTextVillage,
            editTextPostOffice,
            editTextUnion,

            editTextLandDetail,
            editTextLandPrice,
            editTextLandMouza,
            editTextLandJLNo,
            editTextKhatian,
            editTextDagNo,
            editTextLandNature,
            editTextLandSurrounding;

    Spinner spinnerDivision,
            spinnerDistrict,
            spinnerThana,
            spinnerLandType;

    Button btnSubmit;

    String selectedId = "";
    OfflineInfo offlineInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_new_land_add, container, false);

        offlineInfo = new OfflineInfo(getActivity());

        editTextSellerName = rootView.findViewById(R.id.editTextSellerName);
        editTextSellerPhone = rootView.findViewById(R.id.editTextSellerPhone);
        editTextSellerNID = rootView.findViewById(R.id.editTextSellerNID);
        editTextSellerEmail = rootView.findViewById(R.id.editTextSellerEmail);

        editTextHouseNo = rootView.findViewById(R.id.editTextHouseNo);
        editTextRoadNo = rootView.findViewById(R.id.editTextRoadNo);
        editTextVillage = rootView.findViewById(R.id.editTextVillage);
        editTextPostOffice = rootView.findViewById(R.id.editTextPostOffice);
        editTextUnion = rootView.findViewById(R.id.editTextUnion);

        editTextLandDetail = rootView.findViewById(R.id.editTextLandDetail);
        editTextLandPrice = rootView.findViewById(R.id.editTextLandPrice);
        editTextLandMouza = rootView.findViewById(R.id.editTextLandMouza);
        editTextLandJLNo = rootView.findViewById(R.id.editTextLandJLNo);
        editTextKhatian = rootView.findViewById(R.id.editTextKhatian);
        editTextDagNo = rootView.findViewById(R.id.editTextDagNo);
        editTextLandNature = rootView.findViewById(R.id.editTextLandNature);
        editTextLandSurrounding = rootView.findViewById(R.id.editTextLandSurrounding);

        spinnerDivision = rootView.findViewById(R.id.spinnerDivision);
        spinnerDistrict = rootView.findViewById(R.id.spinnerDistrict);
        spinnerThana = rootView.findViewById(R.id.spinnerThana);
        spinnerLandType = rootView.findViewById(R.id.spinnerLandType);

        btnSubmit = rootView.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadSmsDataserver(selectedId,
                        editTextSellerName.getText().toString(),editTextSellerPhone.getText().toString(),
                        editTextSellerNID.getText().toString(),editTextSellerEmail.getText().toString(),
                        editTextHouseNo.getText().toString(),editTextRoadNo.getText().toString(),
                        editTextVillage.getText().toString(),editTextPostOffice.getText().toString(),
                        editTextUnion.getText().toString(),editTextLandDetail.getText().toString(),
                        editTextLandPrice.getText().toString(),editTextLandMouza.getText().toString(),
                        editTextLandJLNo.getText().toString(),editTextKhatian.getText().toString(),
                        editTextDagNo.getText().toString(),editTextLandNature.getText().toString(),
                        editTextLandSurrounding.getText().toString());
            }
        });
        return rootView;
    }

    private void uploadSmsDataserver(String SubCategoryId, String SellerName, String SellerPhone,
                                     String SellerNID, String SellerEmail, String HouseNo, String RoadNo,
                                     String Village, String PostOffice, String Union, String LandDetail,
                                     String LandPrice, String LandMouza, String LandJLNo, String Khatian,
                                     String DagNo, String LandNature, String LandSurrounding) {

        if (editTextSellerName.length() == 0) {
            Toast.makeText(getActivity(), "Title can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editTextSellerPhone.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextSellerNID.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextSellerEmail.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextHouseNo.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextRoadNo.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextVillage.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextPostOffice.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextUnion.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandDetail.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandPrice.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandMouza.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandJLNo.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextKhatian.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextDagNo.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandNature.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (editTextLandSurrounding.length() == 0) {
            Toast.makeText(getActivity(), "Text can not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        String tag_string_req = "req_login";
        ProgressDialog progressDialog = null;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        final ProgressDialog finalProgressDialog = progressDialog;

        AsyncHttpClient client = new AsyncHttpClient();
        //   client.addHeader("Authorization",offlineInfo.getUserInfo().token);

        RequestParams params = new RequestParams();
        params.add("SubCategoryId", SubCategoryId);
        params.add("SellerName", SellerName);
        params.add("SellerPhone", SellerPhone);

        params.add("SellerNID", SellerNID);
        params.add("SellerEmail", SellerEmail);
        params.add("HouseNo", HouseNo);
        params.add("RoadNo", RoadNo);
        params.add("Village", Village);
        params.add("PostOffice", PostOffice);
        params.add("Union", Union);
        params.add("LandDetail", LandDetail);
        params.add("LandPrice", LandPrice);
        params.add("LandMouza", LandMouza);
        params.add("LandJLNo", LandJLNo);
        params.add("Khatian", Khatian);
        params.add("DagNo", DagNo);
        params.add("LandNature", LandNature);
        params.add("LandSurrounding", LandSurrounding);


        System.out.println(params);

        final ProgressDialog finalProgressDialog1 = progressDialog;

        client.post(ServerInfo.BASE_ADDRESS + "land_add_seller", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {

                try {

                        /*String SubCategoryId = response.getString("SubCategoryId");
                        String Title = response.getString("Title");
                        int Text = response.getInt("Text");*/

                    Toast.makeText(getActivity(), "Upload successfully", Toast.LENGTH_LONG).show();


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                finalProgressDialog1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), "Data didn't uploaded " + responseString, Toast.LENGTH_SHORT).show();

            }

        });
    }
}