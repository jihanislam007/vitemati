package md.mazharul.islam.jihan.vitemati.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import md.mazharul.islam.jihan.vitemati.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsViewFragment extends Fragment {


    public DetailsViewFragment() {
        // Required empty public constructor
    }

    TextView ownerNameTV,
            ownerNIDTV,
            ownerMobileNoTV,
            ownerEmailTV,
            divisionNameTV,
            districtNameTV,
            policeStationNameTV,
            houseNoTV,
            roadNoTV,
            villageNameTV,
            postOfficeTV,
            unionTv,
            landTypeTV,
            landPriceTV,
            mouzaNoTV,
            jlNoTV,
            khatianNoTV,
            dagNoTV,
            landNatureTV,
            landSurroundingTV,
            landDetailOtherTV;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_details_view, container, false);

        ownerNameTV = rootView.findViewById(R.id.ownerNameTV);

        ownerNIDTV = rootView.findViewById(R.id.ownerNIDTV);
        ownerMobileNoTV = rootView.findViewById(R.id.ownerMobileNoTV);
        ownerEmailTV = rootView.findViewById(R.id.ownerEmailTV);

        divisionNameTV = rootView.findViewById(R.id.divisionNameTV);
        districtNameTV = rootView.findViewById(R.id.districtNameTV);
        policeStationNameTV = rootView.findViewById(R.id.policeStationNameTV);
        houseNoTV = rootView.findViewById(R.id.houseNoTV);
        roadNoTV = rootView.findViewById(R.id.roadNoTV);
        villageNameTV = rootView.findViewById(R.id.villageNameTV);
        postOfficeTV = rootView.findViewById(R.id.postOfficeTV);
        unionTv = rootView.findViewById(R.id.unionTv);
        landTypeTV = rootView.findViewById(R.id.landTypeTV);
        landPriceTV = rootView.findViewById(R.id.landPriceTV);
        mouzaNoTV = rootView.findViewById(R.id.mouzaNoTV);
        jlNoTV = rootView.findViewById(R.id.jlNoTV);
        khatianNoTV = rootView.findViewById(R.id.khatianNoTV);
        dagNoTV = rootView.findViewById(R.id.dagNoTV);
        landNatureTV = rootView.findViewById(R.id.landNatureTV);
        landSurroundingTV = rootView.findViewById(R.id.landSurroundingTV);
        landDetailOtherTV = rootView.findViewById(R.id.landDetailOtherTV);

        String owner_name = getArguments().getString("owner_name");
        String owner_phone = getArguments().getString("owner_phone");
        String owner_nid = getArguments().getString("owner_nid");
        String owner_email = getArguments().getString("owner_email");

        String division = getArguments().getString("division");
        String district = getArguments().getString("district");
        String policestation = getArguments().getString("policestation");
        String house_no = getArguments().getString("house_no");
        String road_no = getArguments().getString("road_no");
        String village = getArguments().getString("village");
        String post_office = getArguments().getString("post_office");
        String union_ward_no = getArguments().getString("union_ward_no");

        String landtype = getArguments().getString("landtype");
        String property_price = getArguments().getString("property_price");
        String mouza_name = getArguments().getString("mouza_name");
        String jl_no = getArguments().getString("jl_no");
        String khatian = getArguments().getString("khatian");
        String dag_no = getArguments().getString("dag_no");
        String property_size = getArguments().getString("property_size");
        String natureofland = getArguments().getString("natureofland");
        String surroundingofland = getArguments().getString("surroundingofland");



        ownerNameTV.setText(owner_name);
        ownerNIDTV.setText(owner_nid);
        ownerMobileNoTV.setText(owner_phone);
        ownerEmailTV.setText(owner_email);

        divisionNameTV.setText(division);
        districtNameTV.setText(district);
        policeStationNameTV.setText(policestation);
        houseNoTV.setText(house_no);
        roadNoTV.setText(road_no);
        villageNameTV.setText(village);
        postOfficeTV.setText(post_office);
        unionTv.setText(union_ward_no);

        landTypeTV.setText(landtype);
        landPriceTV.setText(property_price);
        mouzaNoTV.setText(mouza_name);
        jlNoTV.setText(jl_no);
        khatianNoTV.setText(khatian);
        dagNoTV.setText(dag_no);
        landNatureTV.setText(property_size);
        landSurroundingTV.setText(surroundingofland);
        landDetailOtherTV.setText(natureofland);

        return rootView;
    }

}
