package md.mazharul.islam.jihan.vitemati.ModelClass;

public class DashBoardListDM {

   String owner_name;
    String owner_phone;
    String owner_nid;
    String owner_email;
    String house_no;
    String road_no;
    String village;
    String post_office;
    String union_ward_no;
    String property_size;
    String property_price;
    String mouza_name;
    String jl_no;
    String khatian;
    String dag_no;
    String natureofland;
    String surroundingofland;
    String longitude;
    String latitude;
    String googlemap_address;
    String division;
    String district;
    String policestation;
    String landtype;


    public DashBoardListDM() {
    }

    public DashBoardListDM(String owner_name, String owner_phone, String owner_nid, String owner_email, String house_no, String road_no, String village, String post_office, String union_ward_no, String property_size, String property_price, String mouza_name, String jl_no, String khatian, String dag_no, String natureofland, String surroundingofland, String longitude, String latitude, String googlemap_address, String division, String district, String policestation, String landtype) {
        this.owner_name = owner_name;
        this.owner_phone = owner_phone;
        this.owner_nid = owner_nid;
        this.owner_email = owner_email;
        this.house_no = house_no;
        this.road_no = road_no;
        this.village = village;
        this.post_office = post_office;
        this.union_ward_no = union_ward_no;
        this.property_size = property_size;
        this.property_price = property_price;
        this.mouza_name = mouza_name;
        this.jl_no = jl_no;
        this.khatian = khatian;
        this.dag_no = dag_no;
        this.natureofland = natureofland;
        this.surroundingofland = surroundingofland;
        this.longitude = longitude;
        this.latitude = latitude;
        this.googlemap_address = googlemap_address;
        this.division = division;
        this.district = district;
        this.policestation = policestation;
        this.landtype = landtype;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwner_phone() {
        return owner_phone;
    }

    public void setOwner_phone(String owner_phone) {
        this.owner_phone = owner_phone;
    }

    public String getOwner_nid() {
        return owner_nid;
    }

    public void setOwner_nid(String owner_nid) {
        this.owner_nid = owner_nid;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public void setOwner_email(String owner_email) {
        this.owner_email = owner_email;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getRoad_no() {
        return road_no;
    }

    public void setRoad_no(String road_no) {
        this.road_no = road_no;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPost_office() {
        return post_office;
    }

    public void setPost_office(String post_office) {
        this.post_office = post_office;
    }

    public String getUnion_ward_no() {
        return union_ward_no;
    }

    public void setUnion_ward_no(String union_ward_no) {
        this.union_ward_no = union_ward_no;
    }

    public String getProperty_size() {
        return property_size;
    }

    public void setProperty_size(String property_size) {
        this.property_size = property_size;
    }

    public String getProperty_price() {
        return property_price;
    }

    public void setProperty_price(String property_price) {
        this.property_price = property_price;
    }

    public String getMouza_name() {
        return mouza_name;
    }

    public void setMouza_name(String mouza_name) {
        this.mouza_name = mouza_name;
    }

    public String getJl_no() {
        return jl_no;
    }

    public void setJl_no(String jl_no) {
        this.jl_no = jl_no;
    }

    public String getKhatian() {
        return khatian;
    }

    public void setKhatian(String khatian) {
        this.khatian = khatian;
    }

    public String getDag_no() {
        return dag_no;
    }

    public void setDag_no(String dag_no) {
        this.dag_no = dag_no;
    }

    public String getNatureofland() {
        return natureofland;
    }

    public void setNatureofland(String natureofland) {
        this.natureofland = natureofland;
    }

    public String getSurroundingofland() {
        return surroundingofland;
    }

    public void setSurroundingofland(String surroundingofland) {
        this.surroundingofland = surroundingofland;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getGooglemap_address() {
        return googlemap_address;
    }

    public void setGooglemap_address(String googlemap_address) {
        this.googlemap_address = googlemap_address;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPolicestation() {
        return policestation;
    }

    public void setPolicestation(String policestation) {
        this.policestation = policestation;
    }

    public String getLandtype() {
        return landtype;
    }

    public void setLandtype(String landtype) {
        this.landtype = landtype;
    }
}
