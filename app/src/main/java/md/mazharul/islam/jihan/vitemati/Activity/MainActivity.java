package md.mazharul.islam.jihan.vitemati.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import md.mazharul.islam.jihan.vitemati.DB.OfflineInfo;
import md.mazharul.islam.jihan.vitemati.Fragment.AboutUsFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.DashBoardFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.DetailsViewFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.LandBuyerFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.LandSaleFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.LogInFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.NewLandAddFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.RegistrationFragment;
import md.mazharul.islam.jihan.vitemati.Fragment.TermsandConditionsFragment;
import md.mazharul.islam.jihan.vitemati.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        titleBar=toolbar.findViewById(R.id.toolbar_title);

        titleBar.setText("ড্যাশবোর্ড");
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        /*////////////////////////////////////////Fragment Looder///////////////*/
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        DashBoardFragment dashBoardFragment = new DashBoardFragment();
        fragmentManager.beginTransaction().add(R.id.frag_container,dashBoardFragment).commit();


        //////////////////////////////////////////////////////////////////
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            // Handle the camera action
            titleBar.setText("ড্যাশবোর্ড");
            DashBoardFragment dashBoardFragment = new DashBoardFragment();
            android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
            fragTransaction.replace(R.id.frag_container,dashBoardFragment );
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();

        } else if (id == R.id.NewLandAddition) {
            titleBar.setText("নতুন জমি সংযোজন");

            OfflineInfo offlineInfo=new OfflineInfo(this);
            if(offlineInfo.getUserInfo()==null){

                Bundle pageInformationBundle=new Bundle();
                pageInformationBundle.putString("page_name","add_land");
//                Gson gson=new Gson();
//                String data=gson.toJson(dashBoardListDMArrayList.get(position));
//                pageInformationBundle.putString("page_data",data);

                LogInFragment logInFragment=new LogInFragment();
                logInFragment.setArguments(pageInformationBundle);

                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,logInFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }else{
                NewLandAddFragment newLandAddFragment = new NewLandAddFragment();

                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,newLandAddFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }



        } else if (id == R.id.Sellersland) {
            titleBar.setText("বিক্রেতার জমি");
            LandSaleFragment landSaleFragment = new LandSaleFragment();



            OfflineInfo offlineInfo=new OfflineInfo(this);
            if(offlineInfo.getUserInfo()==null){

                Bundle pageInformationBundle=new Bundle();
                pageInformationBundle.putString("page_name","seller_land");
//                Gson gson=new Gson();
//                String data=gson.toJson(dashBoardListDMArrayList.get(position));
//                pageInformationBundle.putString("page_data",data);

                LogInFragment logInFragment=new LogInFragment();
                logInFragment.setArguments(pageInformationBundle);

                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,logInFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }else{
                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,landSaleFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }

        } else if (id == R.id.Buyersland) {
            titleBar.setText("ক্রেতার জমি ");

            OfflineInfo offlineInfo=new OfflineInfo(this);
            if(offlineInfo.getUserInfo()==null){

                Bundle pageInformationBundle=new Bundle();
                pageInformationBundle.putString("page_name","buyer");
//                Gson gson=new Gson();
//                String data=gson.toJson(dashBoardListDMArrayList.get(position));
//                pageInformationBundle.putString("page_data",data);

                LogInFragment logInFragment=new LogInFragment();
                logInFragment.setArguments(pageInformationBundle);

                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,logInFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }else{
                LandBuyerFragment landBuyerFragment = new LandBuyerFragment();

                android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frag_container,landBuyerFragment );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }


        } else if (id == R.id.AboutUs) {
            titleBar.setText("আমাদের সম্পর্কে");
            AboutUsFragment aboutUsFragment = new AboutUsFragment();

            android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
            fragTransaction.replace(R.id.frag_container,aboutUsFragment );
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();
        } else if (id == R.id.TermsandConditions) {
            titleBar.setText("শর্তাবলী");
            TermsandConditionsFragment termsandConditionsFragment = new TermsandConditionsFragment();

            android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
            fragTransaction.replace(R.id.frag_container,termsandConditionsFragment );
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();
        }else if (id == R.id.registration) {

            titleBar.setText("");
            RegistrationFragment registrationFragment = new RegistrationFragment();

            android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
            fragTransaction.replace(R.id.frag_container,registrationFragment );
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
