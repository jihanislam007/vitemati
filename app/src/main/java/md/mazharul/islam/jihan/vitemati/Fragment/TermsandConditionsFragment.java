package md.mazharul.islam.jihan.vitemati.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import md.mazharul.islam.jihan.vitemati.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsandConditionsFragment extends Fragment {


    public TermsandConditionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_termsand_conditions, container, false);
    }

}
